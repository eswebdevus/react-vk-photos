import { combineReducers } from "redux";
import { pageReducer } from "./page";
import { userReducer } from "./user";

// global app state (store)
export const rootReducer = combineReducers({
  page: pageReducer,
  user: userReducer
});
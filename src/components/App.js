import React, { Component } from 'react';

import "./App.css";
import UserContainer from "../containers/UserContainer";
import PageContainer from "../containers/PageContainer";

class App extends Component {
  render() {
    return (
      <div className="App">
        <h3 className="App-header">Top Photo</h3>
        <UserContainer />
        <PageContainer />
      </div>
    );
  }
}

export default App;

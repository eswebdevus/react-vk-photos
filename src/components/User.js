import React, {Component} from "react";
import PropTypes from "prop-types";

class User extends Component {
  renderTemplate = () => {
    const {name, error, isFetching} = this.props;

    if (error) {
      return <p>Oops, error! Reload page</p>
    }

    if (isFetching) {
      return <p>Loading...</p>
    }

    if (name) {
      return <p>Hello, {name}</p>
    } else {
      return (
        <button onClick={this.props.handleLogin}>
          Login
        </button>
      )
    }
  };

  render() {
    return <div>{this.renderTemplate()}</div>
  }
}

User.propTypes = {
  name: PropTypes.string.isRequired,
  error: PropTypes.string,
  isFetching: PropTypes.bool.isRequired,
  handleLogin: PropTypes.func.isRequired
};

export default User;
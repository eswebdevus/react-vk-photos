import React, {Component} from "react";
import PropTypes from "prop-types";

class Page extends Component {

  onBtnClick = e => {
    const year = +e.currentTarget.innerText;
    this.props.getPhotos(year);
  };

  renderButtons = () => {
    const years = [2018, 2017, 2016, 2015, 2014]

    return years.map((item, index) => {
      return (
        <button key={index} onClick={this.onBtnClick}>
          {item}
        </button>
      )
    })
  };

  renderTemplate = () => {
    const { photos, isFetching, error } = this.props;

    if (error) {
      return <p>Oops, error!</p>
    }

    if (isFetching) {
      return <p>Loading...</p>
    } else {
      return photos.map(entry => (
        <div key={entry.id} className="photo">
          <p>
            <img src={entry.sizes[0].url} alt="" />
          </p>
          <p>{entry.likes.count} ❤</p>
        </div>
      ))
    }
  };

  render() {
    const {year, photos} = this.props;

    return (
      <div>
        <div>
          <p>{this.renderButtons()}</p>
        </div>
        <h3>
          {year} year [{photos.length}]
        </h3>
        {this.renderTemplate()}
      </div>
    )
  }
}

Page.propTypes = {
  year: PropTypes.number.isRequired,
  photos: PropTypes.array.isRequired,
  getPhotos: PropTypes.func.isRequired,
  error: PropTypes.string,
  isFetching: PropTypes.bool.isRequired
};

export default Page;